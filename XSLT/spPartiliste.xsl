<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes"/>

  <xsl:template match="/">
    <TABLE BORDER="1" BGCOLOR="#ffffff" CELLSPACING="0" CELLPADDING="3">
      <xsl:call-template name="tablehead"/>
      <xsl:apply-templates select="dataroot/partier-fylke"/>
      <xsl:apply-templates select="dataroot/lister-fylke"/>
      <xsl:apply-templates select="dataroot/lister-kommune"/>

    </TABLE>
  </xsl:template>

  <xsl:template match="dataroot/partier-fylke | dataroot/lister-fylke | dataroot/lister-kommune">
    <tr>
      <td>
        <xsl:value-of select="partikode"/>
      </td>
      <td>
        <xsl:value-of select="partinavn"/>
      </td>
      
	<td><xsl:if test="kommnavn != ''"><xsl:value-of select="kommnavn"/></xsl:if>&#160;</td>
	<td align="center">
    <xsl:if test="kommnr != ''"><xsl:value-of select="format-number(kommnr, '0000')"/>
    </xsl:if>&#160;</td>
	<td>
		<xsl:choose>
			<xsl:when test="partikategori='1'">Offisiellt parti</xsl:when>
			<xsl:when test="partikategori='2'">Registrert parti</xsl:when>
			<xsl:otherwise>Liste</xsl:otherwise>
		</xsl:choose>&#160;</td>

    </tr>
  </xsl:template>

  <xsl:template name="tablehead">
    <TR>
      <TH style="width: 2cm">
        <xsl:text>Partikode</xsl:text>
      </TH>
      <TH style="width: 12cm">
        <xsl:text>Partinavn</xsl:text>
      </TH>
	<TH style="width: 6cm">
	<xsl:text>Stiller i</xsl:text>
	</TH>
	<TH style="width: 2cm">
	<xsl:text>Kommunenr.</xsl:text>
	</TH>
	<TH style="width: 3cm">
	<xsl:text>Partitype</xsl:text>
	</TH>

    </TR>
  </xsl:template>

  <xsl:template match="qPartiliste">

  </xsl:template>

</xsl:stylesheet>