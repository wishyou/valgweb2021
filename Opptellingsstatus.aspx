<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Opptellingsstatus.aspx.vb" Inherits="ValgWeb2017.Opptellingsstatus" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Opptellingsstatus</title>
    <link href="Styles.css" type="text/css" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
</head>
<body>
    <form id="mainForm" runat="server">

    <h1>Opptellingsstatus Kommunestyrevalg - Siste rapporteringstidspunkter</h1>
    <p>Inneholder bare kommuner som har rapportert inn resultater.</p>
    <p>Merk: I kommuner med kretsvis rapportering settes status etter den kretsen som er kommet kortest. 
Det betyr at en kommune kan ha status 0 selv om en vesentlig del av stemmene allerede er talt opp.</p>

    Velg fylke:
    <asp:DropDownList AutoPostBack="true" runat="server" ID="selector">
        <asp:ListItem Text="Alle" Value="00"></asp:ListItem> 
        <asp:ListItem Text="Oslo" Value="03"></asp:ListItem> 
        <asp:ListItem Text="Rogaland" Value="11"></asp:ListItem> 
        <asp:ListItem Text="M�re og Romsdal" Value="15"></asp:ListItem> 
        <asp:ListItem Text="Nordland" Value="18"></asp:ListItem> 
        <asp:ListItem Text="Viken" Value="30"></asp:ListItem> 
        <asp:ListItem Text="Innlandet" Value="34"></asp:ListItem> 
        <asp:ListItem Text="Vestfold og Telemark" Value="38"></asp:ListItem> 
        <asp:ListItem Text="Agder" Value="42"></asp:ListItem> 
        <asp:ListItem Text="Vestland" Value="46"></asp:ListItem> 
        <asp:ListItem Text="Tr�ndelag" Value="50"></asp:ListItem> 
        <asp:ListItem Text="Troms og Finnmark" Value="54"></asp:ListItem> 
    </asp:DropDownList>

    <asp:Label runat="server" ID="link"></asp:Label>

    <asp:Xml ID="xmlTransform" runat="server" TransformSource="XSLT/opptellingsstatus.xsl"></asp:Xml>

    </form>
</body>
</html>
